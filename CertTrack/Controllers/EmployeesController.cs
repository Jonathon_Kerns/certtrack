﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CertTrack.Contexts;
using CertTrack.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using System.Text;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace CertTrack.Controllers
{
    //[Authorize(Roles = "CITY\\Fertilizer Certification")]
    [Authorize]
    public class EmployeesController : Controller
    {
        private readonly CertTrackContext _context;

        private readonly IMapper _mapper;

        //public EmployeesController(IMapper mapper) => _mapper = mapper;

        public EmployeesController(CertTrackContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: Employees
        public async Task<IActionResult> Index()
        {
            return View(await _context.Employees.ToListAsync());
        }

        public IActionResult Index_dt()
        {
            return View();
        }

        public async Task<IActionResult> SelectEmployee()
        {

            ViewData["EmployeeId"] = new SelectList(_context.Employees.OrderBy(Employees => Employees.LastName), "EmployeeId", "FullName");
            
            return View();
        }

        public async Task<IActionResult> EmployeeSummary(int? EmployeeId)
        {

            if (EmployeeId == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .FirstOrDefaultAsync(m => m.EmployeeId == EmployeeId);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        // GET: Employees/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        // GET: Employees/Details/5
        public async Task<IActionResult> DetailsSummary(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        // GET: Employees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmployeeId,FirstName,LastName,MiddleInitial,EmpPhone,EmpPhoneExt,EmpEmail,DateOfBirth,StreetAddress1,StreetAddress2,City,AddressState,Zip1,Zip2,EmpCompanyId,ApplicatorTeam")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                _context.Add(employee);
                await _context.SaveChangesAsync();
                //return RedirectToAction(nameof(Index));
                return RedirectToAction("Index", "EmployeeDatum");
            }
            //return View(employee);
            return RedirectToAction("Index", "EmployeeDatum");
        }

        // GET: Employees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                    //.Include(e => e.CompanyEmployees)
                    //.ThenInclude(e => e.Company)
                     .FindAsync(id);
                    //.FirstOrDefaultAsync(i => i.EmployeeId == id.Value);
                    //.SingleOrDefaultAsync(i => i.EmployeeId == id.Value)



            if (employee == null)
            {
                return NotFound();
            }
             return View(employee);
           // return RedirectToAction("Index", "EmployeeDatum");
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EmployeeId,FirstName,LastName,MiddleInitial,EmpPhone,EmpPhoneExt,EmpEmail,DateOfBirth,StreetAddress1,StreetAddress2,City,AddressState,Zip1,Zip2,EmpCompanyId,ApplicatorTeam")] Employee employee)
        {
            if (id != employee.EmployeeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeExists(employee.EmployeeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                // return RedirectToAction(nameof(Index));
                return RedirectToAction("Index", "EmployeeDatum");
            }
            // return View(employee);
            return RedirectToAction("Index", "EmployeeDatum");
        }

        // GET: Employees/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {

                if (id == null)
                {
                    return NotFound();
                }

                var employee = await _context.Employees
                        .FirstOrDefaultAsync(m => m.EmployeeId == id);

                if (employee == null)
                {
                    return NotFound();
                }

              return View(employee);
          //  return RedirectToAction("Index", "EmployeeDatum");

        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employee = await _context.Employees.FindAsync(id);
            _context.Employees.Remove(employee);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {

                return Content("There are still certificate records attached to this employee, they must be deleted first.  Please hit the back button to return to the application.");

            }

            // return RedirectToAction(nameof(Index));
            return RedirectToAction("Index", "EmployeeDatum");
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.EmployeeId == id);
        }

        [HttpPost]
        public IActionResult AjaxMethod()
        {

            List<Employee> employees = _context.Employees.ToList();

            return Json(JsonConvert.SerializeObject(employees));
        }

        // Below are attempts to include Company Name in the index view but they all fail.
        
        [HttpPost]
        // public IActionResult AjaxMethod()
        public async Task<String> AjaxMethod3()
        {

            //List<Employee> employees = _context.Employees
            var employees2 = await _context.Employees
                .Include(e => e.CompanyEmployees)
                .ThenInclude(e => e.Company)
                .ToListAsync();

            //return Json(JsonConvert.SerializeObject(employees));


            var test2 = JsonConvert.SerializeObject(employees2, new JsonSerializerSettings { MaxDepth = 2 });

            return JsonConvert.SerializeObject(employees2, new JsonSerializerSettings { MaxDepth = 3 });
            //return JsonConvert.SerializeObject(employees2);
        }

        /*
        public async Task<IEnumerable<EmployeeViewModel>> AjaxMethod4()
        {
            return await _context.Employees
                .Include(e => e.CompanyEmployees)
                .ThenInclude(e => e.Company)
                .Select(pa => new EmployeeViewModel()
            {
                FirstName = pa.FirstName,
                LastName = pa.LastName,
                CompanyName = pa.Company.CompanyName
            }).ToListAsync();
        }
        */

        //public async Task<IEnumerable<EmployeeViewModel>> AjaxMethod4()
        //public async Task<string> AjaxMethod4()

        /*
        public Task<string> AjaxMethod4()
        {
            /*
            return await _context.Employees
                                         .Include(i => i.CompanyEmployees)
                                         .ThenInclude(i => i.Company)
                                         ._mapper.ProjectTo<EmployeeViewModel>(_context.Set<EmployeeViewModel>())
                                         .ToListAsync();
            */

           // return _mapper.ProjectTo<EmployeeViewModel>(_context.Employees

        /*

              List < EmployeeViewModel > item = _context.Employees
                                     .Include(i => i.CompanyEmployees)
                                     .ThenInclude(i => i.Company)
                                     .ProjectTo<EmployeeViewModel>()
                                     .ToList();

        }

    */

        /*
        [HttpPost]
        // This version is for simple datatables.js views.
        public async Task<String> AjaxMethod()
        {

            var test = await _context.Employees
                 .Include(Employees => Employees.CompanyEmployees)
                 .ThenInclude(CompanyEmployees => CompanyEmployees.Company)
                 .ToListAsync();


            //var test2 = JsonConvert.SerializeObject(test, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Serialize, MaxDepth = 1 });


            return JsonConvert.SerializeObject(test, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Serialize, MaxDepth = 2 });

        }
        */

        // Raw SQL apparently only works with root model data and not with related data.
        [HttpPost]
        public async Task<String> AjaxMethod2()
        {

            //List<Employee> employees = _context.Employees
            var employees = await _context.Employees
                .FromSqlRaw("Execute dbo.GetEmployeeData")
                .ToListAsync();

            //return Json(JsonConvert.SerializeObject(employees));

            var test2 = JsonConvert.SerializeObject(employees, new JsonSerializerSettings { MaxDepth = 4 });

            return JsonConvert.SerializeObject(employees, new JsonSerializerSettings { MaxDepth = 2 });
        }



    }
}
