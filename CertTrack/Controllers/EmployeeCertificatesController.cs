﻿using CertTrack.Contexts;
using CertTrack.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;


namespace CertTrack.Controllers
{
    //[Authorize(Roles = "CITY\\Fertilizer Certification")]
    [Authorize]
    public class EmployeeCertificatesController : Controller
    {
        private readonly CertTrackContext _context;

        public EmployeeCertificatesController(CertTrackContext context)
        {
            _context = context;
        }

        // GET: EmployeeCertificates
        public async Task<IActionResult> Index()
        {
            var certTrackContext = _context.EmployeeCertificates
                .Include(e => e.Certificate)
                .Include(e => e.Employee);
            return View(await certTrackContext.ToListAsync());
        }

        // GET: EmployeeCertificates/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeCertificate = await _context.EmployeeCertificates
                .Include(e => e.Certificate)
                .Include(e => e.Employee)
                .FirstOrDefaultAsync(m => m.EmployeeCertificateId == id);
            if (employeeCertificate == null)
            {
                return NotFound();
            }

            return View(employeeCertificate);
        }

        public async Task<IActionResult> DetailsSummary(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeCertificate = await _context.EmployeeCertificates
                .Include(e => e.Certificate)
                .Include(e => e.Employee)
                .FirstOrDefaultAsync(m => m.EmployeeCertificateId == id);
            if (employeeCertificate == null)
            {
                return NotFound();
            }

            return View(employeeCertificate);
        }

        // GET: EmployeeCertificates/Create
        public IActionResult Create()
        {
            ViewData["CertificateId"] = new SelectList(_context.Certificates, "CertificateId", "CertificateName");
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "EmployeeId", "FullName");
            return View();
        }

        // POST: EmployeeCertificates/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmployeeCertificateId,EmployeeId,CertificateId,CertificateNumber,ApplicatorId,EmpReceiveDate,Comment")] EmployeeCertificate employeeCertificate)
        {
            if (ModelState.IsValid)
            {
                _context.Add(employeeCertificate);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CertificateId"] = new SelectList(_context.Certificates, "CertificateId", "CertificateName", employeeCertificate.CertificateId);
            ViewData["EmployeeId"] = new SelectList(_context.Employees.OrderBy(x => x.LastName), "EmployeeId", "FullName", employeeCertificate.EmployeeId);
            return View(employeeCertificate);
        }

        // GET: EmployeeCertificates/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeCertificate = await _context.EmployeeCertificates.FindAsync(id);
            if (employeeCertificate == null)
            {
                return NotFound();
            }
            ViewData["CertificateId"] = new SelectList(_context.Certificates, "CertificateId", "CertificateName", employeeCertificate.CertificateId);
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "EmployeeId", "FullName", employeeCertificate.EmployeeId);
            return View(employeeCertificate);
        }

        // POST: EmployeeCertificates/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EmployeeCertificateId,EmployeeId,CertificateId,CertificateNumber,ApplicatorId,EmpReceiveDate,Comment")] EmployeeCertificate employeeCertificate)
        {
            if (id != employeeCertificate.EmployeeCertificateId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employeeCertificate);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeCertificateExists(employeeCertificate.EmployeeCertificateId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CertificateId"] = new SelectList(_context.Certificates, "CertificateId", "CertificateName", employeeCertificate.CertificateId);
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "EmployeeId", "FullName", employeeCertificate.EmployeeId);
            return View(employeeCertificate);
        }

        // GET: EmployeeCertificates/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeCertificate = await _context.EmployeeCertificates
                .Include(e => e.Certificate)
                .Include(e => e.Employee)
                .FirstOrDefaultAsync(m => m.EmployeeCertificateId == id);
            if (employeeCertificate == null)
            {
                return NotFound();
            }

            return View(employeeCertificate);
        }

        // POST: EmployeeCertificates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employeeCertificate = await _context.EmployeeCertificates.FindAsync(id);
            _context.EmployeeCertificates.Remove(employeeCertificate);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeCertificateExists(int id)
        {
            return _context.EmployeeCertificates.Any(e => e.EmployeeCertificateId == id);
        }



        [HttpPost]
        //public async Task<IActionResult> AjaxMethod()
        public async Task<String> AjaxMethod()
        {

            Object test = await _context.EmployeeCertificates
                .Include(e => e.Certificate)
                .Include(e => e.Employee)
                .ToListAsync();

            var test2 = JsonConvert.SerializeObject(test, new JsonSerializerSettings { MaxDepth = 2 });

            return JsonConvert.SerializeObject(test, new JsonSerializerSettings { MaxDepth = 2 });

        }

        
        // Based on SelectStandby in Standby application EmployeeStandbyDateController.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SelectYear(int expireYear)
        {

            var test = _context.EmployeeCertificates
            .Where(EmployeeCertificate => EmployeeCertificate.EmpReceiveDate.Year == expireYear - 4)
            .Include(Employee => Employee.Employee)
            .Include(Certificate => Certificate.Certificate)
            .ToList();


            /*
            if (ModelState.IsValid)
            {
                return View("Index2", test);
            }
            return View(test);
            */

            return View("Index2", test);
        }
        

        // This method returns the view with the date picker to allow user to pick a specific Standby Schedule
        public async Task<IActionResult> SelectYear()
        {
            return View();
        }


    }
}
