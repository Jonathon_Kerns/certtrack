﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CertTrack.Contexts;
using CertTrack.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace CertTrack.Controllers
{
    //[Authorize(Roles = "CITY\\Fertilizer Certification")]
    [Authorize]
    public class CompanyEmployeesController : Controller
    {
        private readonly CertTrackContext _context;

        public CompanyEmployeesController(CertTrackContext context)
        {
            _context = context;
        }

        // GET: CompanyEmployees
        public async Task<IActionResult> Index()
        {
            var certTrackContext = _context.CompanyEmployees
                .Include(c => c.Company)
                .Include(c => c.Employee);

         //   var test1 = await certTrackContext.ToListAsync();
            return View(await certTrackContext.ToListAsync());
        }

        // GET: CompanyEmployees/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyEmployee = await _context.CompanyEmployees
                .Include(c => c.Company)
                .Include(c => c.Employee)
                .FirstOrDefaultAsync(m => m.CompanyEmployeeId == id);
            if (companyEmployee == null)
            {
                return NotFound();
            }

            return View(companyEmployee);
        }

        // GET: CompanyEmployees/Details/5
        public async Task<IActionResult> DetailsSummary(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyEmployee = await _context.CompanyEmployees
                .Include(c => c.Company)
                .Include(c => c.Employee)
                .FirstOrDefaultAsync(m => m.CompanyEmployeeId == id);
            if (companyEmployee == null)
            {
                return NotFound();
            }

            return View(companyEmployee);
        }



        // GET: CompanyEmployees/Create
        public IActionResult Create()
        {
            ViewData["CompanyId"] = new SelectList(_context.Companies, "CompanyId", "CompanyName");
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "EmployeeId", "FullName");
            return View();
        }

        // POST: CompanyEmployees/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CompanyEmployeeId,CompanyId,EmployeeId,EmployeeStartDate,EmployeeEndDate,ActiveRecord,EmployeeJobTitle,EmployeeIdatCompany,SupervisorName,SupPhone,SupPhoneExt,SupEmail")] CompanyEmployee companyEmployee)
        {
            if (ModelState.IsValid)
            {
                _context.Add(companyEmployee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "CompanyId", "CompanyName", companyEmployee.CompanyId);
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "EmployeeId", "AddressState", companyEmployee.EmployeeId);
            return View(companyEmployee);
        }

        // GET: CompanyEmployees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyEmployee = await _context.CompanyEmployees.FindAsync(id);
            if (companyEmployee == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "CompanyId", "CompanyName", companyEmployee.CompanyId);
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "EmployeeId", "FullName", companyEmployee.EmployeeId);
            return View(companyEmployee);
        }

        // POST: CompanyEmployees/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CompanyEmployeeId,CompanyId,EmployeeId,EmployeeStartDate,EmployeeEndDate,ActiveRecord,EmployeeJobTitle,EmployeeIdatCompany,SupervisorName,SupPhone,SupPhoneExt,SupEmail")] CompanyEmployee companyEmployee)
        {
            if (id != companyEmployee.CompanyEmployeeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(companyEmployee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyEmployeeExists(companyEmployee.CompanyEmployeeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "CompanyId", "CompanyName", companyEmployee.CompanyId);
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "EmployeeId", "AddressState", companyEmployee.EmployeeId);
            return View(companyEmployee);
        }

        // GET: CompanyEmployees/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyEmployee = await _context.CompanyEmployees
                .Include(c => c.Company)
                .Include(c => c.Employee)
                .FirstOrDefaultAsync(m => m.CompanyEmployeeId == id);
            if (companyEmployee == null)
            {
                return NotFound();
            }

            return View(companyEmployee);
        }

        // POST: CompanyEmployees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var companyEmployee = await _context.CompanyEmployees.FindAsync(id);
            _context.CompanyEmployees.Remove(companyEmployee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompanyEmployeeExists(int id)
        {
            return _context.CompanyEmployees.Any(e => e.CompanyEmployeeId == id);
        }

        [HttpPost]
        //public async Task<IActionResult> AjaxMethod()
        public async Task<String> AjaxMethod()
        {

            Object test = await _context.CompanyEmployees
                .Include(e => e.Employee)
                .Include(e => e.Company)
                .ToListAsync();

           // var test2 = JsonConvert.SerializeObject(test, new JsonSerializerSettings { MaxDepth = 3 });


            return JsonConvert.SerializeObject(test, new JsonSerializerSettings { MaxDepth = 3 });

        }

    }
}
