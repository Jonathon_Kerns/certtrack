﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CertTrack.Contexts;
using CertTrack.Models;

namespace CertTrack.Controllers
{
    //[Authorize(Roles = "CITY\\Fertilizer Certification")]
    [Authorize]
    public class CertificationSummaryTotalsController : Controller
    {
        private readonly CertTrackContext _context;

        public CertificationSummaryTotalsController(CertTrackContext context)
        {
            _context = context;
        }

        // GET: CertificationSummaryTotals
        public async Task<IActionResult> Index()
        {
            return View(await _context.CertificationSummaryTotals.ToListAsync());
        }

     
    }
}
