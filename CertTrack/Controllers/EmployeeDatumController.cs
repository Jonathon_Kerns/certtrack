﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CertTrack.Contexts;
using CertTrack.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

// This controller works with a database view.

namespace CertTrack.Controllers
{
    //[Authorize(Roles = "CITY\\Fertilizer Certification")]
    [Authorize]
    public class EmployeeDatumController : Controller
    {
        private readonly CertTrackContext _context;

        public EmployeeDatumController(CertTrackContext context)
        {
            _context = context;
        }

        
        public async Task<IActionResult> Index()
        {
           // var test1 = await _context.EmployeeDatum.ToListAsync();
            return View(await _context.EmployeeDatum.ToListAsync());
        }
        


        //   Use this to print out the query string to console for debugging....
         
        /*
        public List<CertTrack.Models.EmployeeData> Index()
        {
            var query = _context.EmployeeDatum;

            var queryString = query.ToQueryString();

            Console.WriteLine(queryString);

            return query.ToList();

        }
        */

        public async Task<IActionResult> Index2()
        {
            return View();
        }


        [HttpPost]
        public async  Task<IActionResult> AjaxMethod()
        {

            List<EmployeeData> employeedatum = await _context.EmployeeDatum.ToListAsync();

            var Jsontest1 = Json(JsonConvert.SerializeObject(employeedatum));

            return Json(JsonConvert.SerializeObject(employeedatum));
        }



    }


}
