﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CertTrack.Models
{
    [Keyless]
    [Table("CertificationSummaryTotal")]
    public partial class CertificationSummaryTotal
    {
        [Display(Name = "One Certification Ever")]
        public int? OneCertEver { get; set; }
        [Display(Name = "Multiple Certifications Ever")]
        public int? MultiCertEver { get; set; }
        public int? Total { get; set; }
        public int? CertSumTotalKey { get; set; }
    }
}
