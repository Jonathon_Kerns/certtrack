﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CertTrack.Models
{
    [Keyless]
    [Table("CertificationSummary")]
    public partial class CertificationSummary
    {
        [StringLength(4)]
        public string Year { get; set; }
        [Display(Name = "One Certification")]
        public int? OnceCertCount { get; set; }
        [Display(Name = "Multiple Certifications")]
        public int? MoreCertCount { get; set; }
        public int? Total { get; set; }
    }
}
