﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CertTrack.Models
{
    [Table("Company")]
    public partial class Company
    {
        public Company()
        {
            CompanyEmployees = new HashSet<CompanyEmployee>();
			//Employees = new HashSet<Employee>();
        }

        public string ContactName => string.Format("{0}, {1}", PrimaryContactLast, PrimaryContactFirst);

        [Key]
        [Display(Name = "Company ID")]
        public int CompanyId { get; set; }
        [Required]
        [StringLength(500)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [StringLength(255)]
        [Display(Name = "Branch")]
        public string CompanyBranchName { get; set; }
        [StringLength(255)]
        [Display(Name = "Address 1")]
        public string StreetAddress1 { get; set; }
        [StringLength(255)]
        [Display(Name = "Address 2")]
        public string StreetAddress2 { get; set; }
        [StringLength(50)]
        public string City { get; set; }
        [StringLength(2)]
        [Display(Name = "State")]
        public string AddressState { get; set; }
        [StringLength(5)]
        public string Zip1 { get; set; }
        [StringLength(4)]
        public string Zip2 { get; set; }
        [StringLength(50)]
        [Display(Name = "Primary Contact First Name")]
        public string PrimaryContactFirst { get; set; }
        [StringLength(50)]
        [Display(Name = "Primary Contact Last Name")]
        public string PrimaryContactLast { get; set; }
        [StringLength(10)]
        [Display(Name = "Primary Contact Phone")]
        public string PrimaryContactPhone { get; set; }
        [StringLength(10)]
        [Display(Name = "Primary Contact Extension")]
        public string PrimaryContactPhoneExt { get; set; }
        [StringLength(255)]
        [Display(Name = "Primary Contact Email")]
        public string PrimaryContactEmail { get; set; }

        [InverseProperty(nameof(CompanyEmployee.Company))]
        [Newtonsoft.Json.JsonIgnore] // This attribute helps limit the depth of json nesting in the return file to datatables.js
        public virtual ICollection<CompanyEmployee> CompanyEmployees { get; set; }

    }
}
