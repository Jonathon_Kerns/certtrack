﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CertTrack.Models
{
    [Table("CompanyEmployee")]
    public partial class CompanyEmployee
    {
        [Key]
        public int CompanyEmployeeId { get; set; }
        [Column("CompanyID")]
        public int CompanyId { get; set; }
        [Display(Name = "Employee ID")]
        [Column("EmployeeID")]
        public int EmployeeId { get; set; }
        [Display(Name = "Employee Start Date")]
        [Column(TypeName = "Date")]
        [Microsoft.AspNetCore.Mvc.BindProperty, DataType(DataType.Date)]
        public DateTime EmployeeStartDate { get; set; }
        [Column(TypeName = "Date")]
        [Microsoft.AspNetCore.Mvc.BindProperty, DataType(DataType.Date)]
        [Display(Name = "Employee End Date")]
        public DateTime? EmployeeEndDate { get; set; }
        [Display(Name = "Active")]
        public bool ActiveRecord { get; set; }
        [StringLength(500)]
        [Display(Name = "Employee Job Title")]
        public string EmployeeJobTitle { get; set; }
        [Display(Name = "Employee Company ID")]
        [Column("EmployeeIDatCompany")]
        [StringLength(100)]
        public string EmployeeIdatCompany { get; set; }
        [StringLength(103)]
        [Display(Name = "Supervisor Name")]
        public string SupervisorName { get; set; }
        [StringLength(10)]
        [Display(Name = "Supervisor Phone")]
        public string SupPhone { get; set; }
        [StringLength(10)]
        [Display(Name = "Supervisor Extension")]
        public string SupPhoneExt { get; set; }
        [StringLength(255)]
        [Display(Name = "Supvervisor Email")]
        public string SupEmail { get; set; }

        [ForeignKey(nameof(CompanyId))]
        [InverseProperty("CompanyEmployees")]
        //[Newtonsoft.Json.JsonIgnore]                  // Seems to need to be commented out for Employee and CompanyEmployee DT reports
        public virtual Company Company { get; set; }
        [ForeignKey(nameof(EmployeeId))]
        [InverseProperty("CompanyEmployees")]
        //[Newtonsoft.Json.JsonIgnore]                  // With these commented out, all Datatables reports work except Employee
        public virtual Employee Employee { get; set; }
    }
}
