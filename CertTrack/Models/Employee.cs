﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CertTrack.Models
{
    [Table("Employee")]
    public partial class Employee
    {
        public Employee()
        {
            CompanyEmployees = new HashSet<CompanyEmployee>();
            EmployeeCertificates = new HashSet<EmployeeCertificate>();
        }


        [Key]
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Employee First Name")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Employee Last Name")]
        public string LastName { get; set; }
        [StringLength(1)]
        [Display(Name = "Employee Middle Initial")]
        public string MiddleInitial { get; set; }
        [StringLength(10)]
        [Display(Name = "Employee Phone")]
        public string EmpPhone { get; set; }
        [StringLength(10)]
        [Display(Name = "Employee Extension")]
        public string EmpPhoneExt { get; set; }
        [StringLength(255)]
        [Display(Name = "Employee Email")]
        public string EmpEmail { get; set; }
        [Microsoft.AspNetCore.Mvc.BindProperty, DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Column(TypeName = "Date")]
        [Display(Name = "Employee DOB")]
        public DateTime? DateOfBirth { get; set; }
        [Required]
        [StringLength(255)]
        [Display(Name = "Employee Address 1")]
        public string StreetAddress1 { get; set; }
        [StringLength(255)]
        [Display(Name = "Employee Address 2")]
        public string StreetAddress2 { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }
        [Required]
        [StringLength(2)]
        [Display(Name = "State")]
        public string AddressState { get; set; }
        [Required]
        [StringLength(5)]
        public string Zip1 { get; set; }
        [StringLength(4)]
        public string Zip2 { get; set; }
        [Display(Name = "Employee ID at Company")]
        [Column("EmpCompanyID")]
        public string EmpCompanyId { get; set; }
        [StringLength(255)]
        [Display(Name = "Applicator Team")]
        public string ApplicatorTeam { get; set; }

        /*
        [StringLength(500)]
        [Display(Name = "EmployeeComment")]
        public string EmployeeComment { get; set; }
        */
        /*
        [Newtonsoft.Json.JsonIgnore]   //Use this attribute to help control the depth of json serialization.
		[ForeignKey(nameof(EmpCompanyId))]
        [InverseProperty(nameof(Company.Employees))]
        public virtual Company EmpCompany { get; set; }
        */

        // TESTING, THIS MAY BREAK SOMETHING

        [InverseProperty(nameof(CompanyEmployee.Employee))]
        [Newtonsoft.Json.JsonIgnore]     // If you comment this out the Employees Datatables report doesn't work at all
        public virtual ICollection<CompanyEmployee> CompanyEmployees { get; set; }

        [InverseProperty(nameof(EmployeeCertificate.Employee))]
        [Newtonsoft.Json.JsonIgnore]   //Use this attribute to help control the depth of json serialization.
        public virtual ICollection<EmployeeCertificate> EmployeeCertificates { get; set; }
    }
}
