﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;


#nullable disable

namespace CertTrack.Models
{
    [Keyless]
    public partial class EmployeeData
    {
       // public int CompanyId { get; set; }
        
        [StringLength(500)]
        public string CompanyName { get; set; }
      //  [StringLength(255)]
     //   public string CompanyBranchName { get; set; }
        [StringLength(255)]
        public string StreetAddress1 { get; set; }
        [StringLength(255)]
        public string StreetAddress2 { get; set; }
        [StringLength(50)]
        public string City { get; set; }
        [StringLength(2)]
        public string AddressState { get; set; }
        [StringLength(5)]
        public string Zip1 { get; set; }
        [StringLength(4)]
        public string Zip2 { get; set; }
        //[StringLength(50)]
        //public string PrimaryContactFirst { get; set; }
        //[StringLength(50)]
        //public string PrimaryContactLast { get; set; }
        //[StringLength(10)]
        //public string PrimaryContactPhone { get; set; }
        //[StringLength(10)]
        //public string PrimaryContactPhoneExt { get; set; }
        //[StringLength(255)]
        //public string PrimaryContactEmail { get; set; }
        //public int CompanyEmployeeId { get; set; }

       // public int Expr1 { get; set; }
        public int EmployeeId { get; set; }
       // [Column(TypeName = "date")]
       // public DateTime EmployeeStartDate { get; set; }
       // [Column(TypeName = "date")]
       // public DateTime? EmployeeEndDate { get; set; }
      //  public bool ActiveRecord { get; set; }
       // [StringLength(500)]
       // public string EmployeeJobTitle { get; set; }
        //[Column("EmpCompanyId")]
       // [Column("EmployeeIDatCompany")]
       // [StringLength(100)]
       // public string EmployeeIdatCompany { get; set; }
        //[StringLength(103)]
       // public string SupervisorName { get; set; }
       // [StringLength(10)]
       // public string SupPhone { get; set; }
       // [StringLength(10)]
       // public string SupPhoneExt { get; set; }
       // [StringLength(255)]
       // public string SupEmail { get; set; }
       // public int Expr2 { get; set; }
        
        [StringLength(50)]
        public string FirstName { get; set; }
        
        [StringLength(50)]
        public string LastName { get; set; }
        [StringLength(1)]
        public string MiddleInitial { get; set; }
        [StringLength(10)]
        public string EmpPhone { get; set; }
        [StringLength(10)]
        public string EmpPhoneExt { get; set; }
        [StringLength(255)]
        public string EmpEmail { get; set; }
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Microsoft.AspNetCore.Mvc.BindProperty, DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }



        // [StringLength(255)]
        //  [Display(Name = "Employee Address1")]
        // public string Expr3 { get; set; }
        // [StringLength(255)]
        // public string Expr4 { get; set; }

        // [StringLength(50)]
        // public string Expr5 { get; set; }

        // [StringLength(2)]
        // public string Expr6 { get; set; }

        // [StringLength(5)]
        // public string Expr7 { get; set; }
        // [StringLength(4)]
        // public string Expr8 { get; set; }

        // public int EmpCompanyId { get; set; }
        [StringLength(255)]
        public string ApplicatorTeam { get; set; }

        
        [StringLength(500)]
        [Display(Name = "EmployeeComment")]
        public string EmployeeComment { get; set; }
        
        }
}
