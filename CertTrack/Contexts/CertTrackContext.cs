﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using CertTrack.Models;

#nullable disable

namespace CertTrack.Contexts
{
    public partial class CertTrackContext : DbContext
    {
        public CertTrackContext()
        {
        }

        public CertTrackContext(DbContextOptions<CertTrackContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Certificate> Certificates { get; set; }
        public virtual DbSet<CertificationSummary> CertificationSummaries { get; set; }
        public virtual DbSet<CertificationSummaryTotal> CertificationSummaryTotals { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<CompanyEmployee> CompanyEmployees { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmployeeCertificate> EmployeeCertificates { get; set; }
        public virtual DbSet<EmployeeData> EmployeeDatum { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=COTSQLT\\SQL2019;Initial Catalog=CertTrack;Persist Security Info=True;User ID=uucert;Password=Uucert_test");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Certificate>(entity =>
            {
                entity.Property(e => e.CertificateDescription).IsUnicode(false);

                entity.Property(e => e.CertificateName).IsUnicode(false);

                entity.Property(e => e.CertificationApplicationId).IsUnicode(false);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.AddressState).IsUnicode(false);

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.CompanyBranchName).IsUnicode(false);

                entity.Property(e => e.CompanyName).IsUnicode(false);

                entity.Property(e => e.PrimaryContactEmail).IsUnicode(false);

                entity.Property(e => e.PrimaryContactFirst).IsUnicode(false);

                entity.Property(e => e.PrimaryContactLast).IsUnicode(false);

                entity.Property(e => e.PrimaryContactPhone)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.PrimaryContactPhoneExt)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.StreetAddress1).IsUnicode(false);

                entity.Property(e => e.StreetAddress2).IsUnicode(false);

                entity.Property(e => e.Zip1).IsUnicode(false);

                entity.Property(e => e.Zip2).IsUnicode(false);
            });

            modelBuilder.Entity<CompanyEmployee>(entity =>
            {
                entity.Property(e => e.EmployeeIdatCompany).IsUnicode(false);

                entity.Property(e => e.EmployeeJobTitle).IsUnicode(false);

                entity.Property(e => e.SupEmail).IsUnicode(false);

                entity.Property(e => e.SupPhone)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.SupPhoneExt)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.SupervisorName).IsUnicode(false);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.CompanyEmployees)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CompanyEm__Compa__403A8C7D");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.CompanyEmployees)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CompanyEm__Emplo__412EB0B6");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.Property(e => e.AddressState).IsUnicode(false);

                entity.Property(e => e.ApplicatorTeam).IsUnicode(false);

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.EmpEmail).IsUnicode(false);

                entity.Property(e => e.EmpPhone)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.EmpPhoneExt)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.FirstName).IsUnicode(false);

                entity.Property(e => e.LastName).IsUnicode(false);

                entity.Property(e => e.MiddleInitial).IsUnicode(false);

                entity.Property(e => e.StreetAddress1).IsUnicode(false);

                entity.Property(e => e.StreetAddress2).IsUnicode(false);

                entity.Property(e => e.Zip1).IsUnicode(false);

                entity.Property(e => e.Zip2).IsUnicode(false);

               // entity.Property(e => e.EmployeeComment).IsUnicode(false);

                /*
                entity.HasOne(d => d.EmpCompany)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.EmpCompanyId)
                    .HasConstraintName("FK__Employee__EmpCom__4222D4EF");
                */
            });

            modelBuilder.Entity<EmployeeCertificate>(entity =>
            {
                entity.Property(e => e.ApplicatorId).IsUnicode(false);

                entity.Property(e => e.Comment).IsUnicode(false);

                entity.HasOne(d => d.Certificate)
                    .WithMany(p => p.EmployeeCertificates)
                    .HasForeignKey(d => d.CertificateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__EmployeeC__Certi__3C69FB99");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.EmployeeCertificates)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__EmployeeC__Emplo__628FA481");

            });

                modelBuilder.Entity<EmployeeData>(entity =>
                {
                    entity.ToView("EmployeeData");

                    entity.Property(e => e.AddressState).IsUnicode(false);

                    entity.Property(e => e.ApplicatorTeam).IsUnicode(false);

                    entity.Property(e => e.City).IsUnicode(false);

                   // entity.Property(e => e.EmployeeComment).IsUnicode(false);


                    entity.Property(e => e.CompanyName).IsUnicode(false);


                    entity.Property(e => e.EmpEmail).IsUnicode(false);

                    entity.Property(e => e.EmpPhone)
                        .IsUnicode(false)
                        .IsFixedLength(true);

                    entity.Property(e => e.EmpPhoneExt)
                        .IsUnicode(false)
                        .IsFixedLength(true);

                    entity.Property(e => e.FirstName).IsUnicode(false);

                    entity.Property(e => e.LastName).IsUnicode(false);

                    entity.Property(e => e.MiddleInitial).IsUnicode(false);

                    /*
                    entity.Property(e => e.PrimaryContactEmail).IsUnicode(false);

                    entity.Property(e => e.PrimaryContactFirst).IsUnicode(false);

                    entity.Property(e => e.PrimaryContactLast).IsUnicode(false);

                    entity.Property(e => e.PrimaryContactPhone)
                        .IsUnicode(false)
                        .IsFixedLength(true);

                    entity.Property(e => e.PrimaryContactPhoneExt)
                        .IsUnicode(false)
                        .IsFixedLength(true);

                    */

                    entity.Property(e => e.StreetAddress1).IsUnicode(false);

                    entity.Property(e => e.StreetAddress2).IsUnicode(false);


                    entity.Property(e => e.Zip1).IsUnicode(false);

                    entity.Property(e => e.Zip2).IsUnicode(false);

            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
