﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CertTrack.Models
{
    [Table("Certificate")]
    public partial class Certificate
    {
        public Certificate()
        {
            EmployeeCertificates = new HashSet<EmployeeCertificate>();
        }

        [Key]
        [Display(Name ="Certificate ID")]
        public int CertificateId { get; set; }
        [Required]
        [StringLength(255)]
        [Display(Name = "Certificate Name")]
        public string CertificateName { get; set; }
        [Display(Name = "Certificate Application ID")]
        [Column("CertificationApplicationID")]
        [StringLength(255)]
        public string CertificationApplicationId { get; set; }
        [StringLength(500)]
        [Display(Name = "Certificate Description")]
        public string CertificateDescription { get; set; }
        [Display(Name = "Days Valid")]
        public int DaysValid { get; set; }

        [Newtonsoft.Json.JsonIgnore]  //Use this attribute to help control the depth of json serialization.
        [InverseProperty(nameof(EmployeeCertificate.Certificate))]
        public virtual ICollection<EmployeeCertificate> EmployeeCertificates { get; set; }
    }
}
