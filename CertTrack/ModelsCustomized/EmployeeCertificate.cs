﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CertTrack.Models
{
    [Table("EmployeeCertificate")]
    public partial class EmployeeCertificate
    {
        [Key]
        public int EmployeeCertificateId { get; set; }
        //[Column("EmployeeID")]
        [Display(Name = "Employee ID")]
        public int EmployeeId { get; set; }
        //[Column("CertificateID")]
        [Display(Name = "Certificate ID")]
        public int CertificateId { get; set; }
        [Display(Name = "Certificate Number")]
        public int? CertificateNumber { get; set; }
        [Display(Name = "Applicator ID")]
        [Column("ApplicatorID")]
        [StringLength(255)]
        public string ApplicatorId { get; set; }
        [Display(Name = "Issue Date")]
        [Column(TypeName = "Date")]
        [Microsoft.AspNetCore.Mvc.BindProperty, DataType(DataType.Date)]
       // [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EmpReceiveDate { get; set; }
        [StringLength(500)]
        public string Comment { get; set; }

        [ForeignKey(nameof(CertificateId))]
        [InverseProperty("EmployeeCertificates")]
        //[Newtonsoft.Json.JsonIgnore]  //Use this attribute to help control the depth of json serialization.
        public virtual Certificate Certificate { get; set; }
        [ForeignKey(nameof(EmployeeId))]
        [InverseProperty("EmployeeCertificates")]
        //[Newtonsoft.Json.JsonIgnore]
        public virtual Employee Employee { get; set; }
    }
}
